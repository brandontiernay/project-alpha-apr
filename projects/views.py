from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProjectForm


# View that will get all instances of the Project model
@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": projects,
    }
    return render(request, "projects/list.html", context)


# View that shows the details of a particular project
@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {
        "show_project": project,
    }
    return render(request, "projects/details.html", context)


# View function for 'Project' model to create a new project.
@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = CreateProjectForm()

    context = {
        "form": form,
    }

    return render(request, "projects/create.html", context)

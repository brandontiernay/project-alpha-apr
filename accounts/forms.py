from django import forms


class UserLoginForm(forms.Form):
    username = forms.CharField(max_length=150, required=True)
    password = forms.CharField(
        max_length=150,
        required=True,
        widget=forms.PasswordInput,
    )


class UserSignupForm(forms.Form):
    username = forms.CharField(max_length=150, required=True)
    password = forms.CharField(
        max_length=150,
        required=True,
        widget=forms.PasswordInput,
    )
    password_confirmation = forms.CharField(
        max_length=150,
        required=True,
        widget=forms.PasswordInput,
    )
